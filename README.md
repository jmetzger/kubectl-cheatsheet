# Kubectl 

### Nodes ###
  
  * Which hardware (Bare metal, virtual macines) are part of your cluster ? 

```bash 
# Which machines are part of the cluster 
kubectl get nodes 
```

### Namespaces ###

  * Which namespaces exit on your system 
  * default will be for your workers 

```bash
kubectl get namespaces 

NAME              STATUS   AGE
default           Active   23h # worker namespace
kube-node-lease   Active   23h 
kube-public       Active   23h
kube-system       Active   23h # kubernetes base system

```

### DNS (Kubernetes) installed ?

  * DNS is needed to dns-names for services
  ```bash
  $ kubectl get services kube-dns --namespace=kube-system
  NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
  kube-dns   ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   23h
  ```


## Working with config 

``` bash
# Work with the config 
kubectl config 
# view current config 
kubectl config view

```

### Applying a complete folder (kustomization)

``` bash
# File within needs to be 
# kustomization.yml 

# Setup project / setting resources / starting
kubectl apply -k ./  # currentfolder 
kubectl apply -k path/to/folder 

# Tearing down project again 
kubectl delete -k ./
```

### Applying a remote configure 

```bash
kubectl apply -f https://k8s.io/examples/controllers/nginx-deployment.yaml
```

### Working with secrets 

```bash
kubectl get secrets 
```

```yaml
# secrets are created like so in the config (yml) - file
secretGenerator:
- name: mysql-pass
  literals:
  - password=YOUR_PASSWORD
```



### Exec + Exec (Interactively mit Terminal)

  * Execute a command on a pod (but what is there multiple containers in a pod ?)
  ```bash
  kubectl exec my_pod -- date
  kubectl exec my_pod -- ls -la
  ```

  * When you want to connect to the pod interactively with a terminal:
  ```
  kubectl exec -it my_pod -- bash 


### Example nginx ###

```
# Setup nginx 
kubectl apply -f https://k8s.io/examples/controllers/nginx-deployment.yaml
```

```bash
# now pods are available 
kubectl get pods

# no services yet 
kubectl get services

# what pods are running now
kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-66b6c48dd5-6r27w   1/1     Running   0          5m56s
nginx-deployment-66b6c48dd5-7kmgf   1/1     Running   0          5m56s
nginx-deployment-66b6c48dd5-7q8sg   1/1     Running   0          5m56s

ubuntu@k8-master:~/nginx-test$ kubectl delete pod nginx-deployment-66b6c48dd5-6r27w 
pod "nginx-deployment-66b6c48dd5-6r27w" deleted
# It will trigger a new pod 
ubuntu@k8-master:~/nginx-test$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-66b6c48dd5-7kmgf   1/1     Running   0          7m6s
nginx-deployment-66b6c48dd5-7q8sg   1/1     Running   0          7m6s
nginx-deployment-66b6c48dd5-9l8fs   1/1     Running   0          4s
ubuntu@k8-master:~/nginx-test$ 
```

```
## Delete nginx 
kubectl delete -f https://k8s.io/examples/controllers/nginx-deployment.yaml
```

### Rollout 

  * Currently i have no idea what this does

  ``` bash
  kubectl rollout status deployment.v1.apps/nginx-deployment
  ```

  * Some refs to this migt be here: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/









